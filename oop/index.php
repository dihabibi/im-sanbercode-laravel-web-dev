<?php
require_once('Animal.php');
require_once('Ape.php');
require_once('Frog.php');

$sheep = new Animal("Shaun");
echo "Name: " . $sheep->getName() . "<br>"; // "Shaun"
echo "Legs: " . $sheep->getLegs() . "<br>"; // 4
echo "Cold Blooded: " . $sheep->getColdBlooded() . "<br>"; // "no"
echo "<br>";

$kodok = new Frog("Buduk");
echo "Name: " . $kodok->getName() . "<br>"; // "Buduk"
echo "Legs: " . $kodok->getLegs() . "<br>"; // 4
echo "Cold Blooded: " . $kodok->getColdBlooded() . "<br>"; // "no"
echo "Jump: ";
$kodok->jump() . "<br>"; // "hop hop"
echo "<br>";
echo "<br>";

$sungokong = new Ape("Kera Sakti");
echo "Name: " . $sungokong->getName() . "<br>"; // "Kera Sakti"
echo "Legs: " . $sungokong->getLegs() . "<br>"; // 2
echo "Cold Blooded: " . $sungokong->getColdBlooded() . "<br>"; // "no"
echo "Yell: ";
$sungokong->yell(); // "Auooo"
echo "<br>";
