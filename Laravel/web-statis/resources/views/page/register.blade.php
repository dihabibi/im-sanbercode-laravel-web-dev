@extends('layouts.master')

@section('title')
Sign Up Form
@endsection

@section('content')
    <form action="welcome" method="POST">
      @csrf
      <label for="first-name">First name:</label><br />
      <input type="text" id="first-name" name="first-name" /><br /><br />

      <label for="last-name">Last name:</label><br />
      <input type="text" id="last-name" name="last-name" /><br /><br />

      <label>Gender:</label><br />
      <input type="radio" id="gender-male" name="gender" value="male" />
      <label for="gender-male">Male</label><br />
      <input type="radio" id="gender-female" name="gender" value="female" />
      <label for="gender-female">Female</label><br />
      <input type="radio" id="gender-other" name="gender" value="other" />
      <label for="gender-other">Other</label><br /><br />

      <label for="nationality">Nationality:</label>
      <select id="nationality" name="nationality">
        <option value="indonesia">Indonesia</option></select
      ><br /><br />

      <label for="language">Language Spoken:</label><br />
      <input
        type="checkbox"
        id="language-indonesian"
        name="language"
        value="indonesian"
      />
      <label for="language-indonesian">Bahasa Indonesia</label><br />
      <input
        type="checkbox"
        id="language-english"
        name="language"
        value="english"
      />
      <label for="language-english">English</label><br />
      <input
        type="checkbox"
        id="language-other"
        name="language"
        value="other"
      />
      <label for="language-other">Other</label><br /><br />

      <label for="bio">Bio:</label><br />
      <textarea id="bio" name="bio" rows="4" cols="50"></textarea><br /><br />

      <input type="submit" value="Sign Up" />
    </form>
@endsection