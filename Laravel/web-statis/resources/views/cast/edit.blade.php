@extends('layouts.master')

@section('title')
  Edit Cast
@endsection

@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
  @csrf
  @method('PUT')
  <div class="form-group">
    <label for="nama">Nama</label>
    <input type="text"  name='nama'  value="{{$cast-> nama}}" class="@error('nama') is-invalid @enderror form-control" >
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

  <div class="form-group">
    <label for="umur">Umur</label>
    <input type="number" name="umur" value="{{$cast-> umur}}" class="@error('umur') is-invalid @enderror form-control">
  </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
 
  <div class="form-group">
    <label for="bio">Biodata</label>
    <textarea type="text" name="bio" class="@error('bio') is-invalid @enderror form-control" cols="30" rows="10">{{$cast-> bio}}</textarea>
  </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection