@extends('layouts.master')

@section('title')
  Tambah Cast
@endsection

@section('content')

<form action="/cast" method="POST">
  @csrf
  <div class="form-group">
    <label for="nama">Nama</label>
    <input type="text"  name='nama' class="@error('nama') is-invalid @enderror form-control" >
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

  <div class="form-group">
    <label for="umur">Umur</label>
    <input type="number" name="umur" class="@error('umur') is-invalid @enderror form-control">
  </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
 
  <div class="form-group">
    <label for="bio">Biodata</label>
    <textarea type="text" name="bio" class="@error('bio') is-invalid @enderror form-control" cols="30" rows="10"></textarea>
  </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection