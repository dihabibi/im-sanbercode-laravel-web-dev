@extends('layouts.master')

@section('title')
  Detail Cast
@endsection

@section('content')

    <div class="card-header">
      <h3 class="card-title">{{$cast->nama}}</h3>
    </div>
    <div class="card-body">
      <p><strong>Umur:</strong> {{$cast->umur}}</p>
      <p><strong>Biodata:</strong> {{$cast->bio}}</p>
    </div>

@endsection
