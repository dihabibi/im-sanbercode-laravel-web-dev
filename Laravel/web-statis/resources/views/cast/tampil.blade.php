@extends('layouts.master')

@section('title')
  Tampil Cast
@endsection

@section('content')

    <a href="/cast/create" class="btn btn-sm btn-primary">Tambah Cast</a>
    <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($casts as $cast)
          <tr>
            <th scope="row">{{ $loop->iteration }}</th>
            <td>{{ $cast->nama }}</td>
            <td>
                <a href="/cast/{{ $cast->id }}" class="btn btn-sm btn-primary">Detail</a>
                <a href="/cast/{{ $cast->id }}/edit" class="btn btn-sm btn-warning">Edit</a>
                <form action="/cast/{{ $cast->id }}" method="POST" class="d-inline">
                @csrf
                @method('delete')
                <button type="submit" class="btn btn-sm btn-danger">Delete</button>
              </form>
            </td>
          </tr>
          @empty
          <tr>
            <th colspan="5" class="text-center">Data Kosong</th>
          </tr>
          @endforelse
        </tbody>
    </table>

@endsection
