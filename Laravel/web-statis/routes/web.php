<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get("/", [MenuController::class, "dashboard"]);
Route::get("/table", [MenuController::class, "table"]);
Route::get("/data-tables", [MenuController::class, "dataTables"]);

Route::get("/register", [AuthController::class, "register"]);
Route::post("/welcome", [AuthController::class, "welcome"]);

// create
Route::get("/cast/create", [CastController::class, "create"]);
Route::post("/cast", [CastController::class, "store"]);

// read
Route::get("/cast", [CastController::class, "index"]);
Route::get("/cast/{id}", [CastController::class, "show"]);

// update
Route::get("/cast/{id}/edit", [CastController::class, "edit"]);
Route::put("/cast/{id}", [CastController::class, "update"]);

// delete
Route::delete("/cast/{id}", [CastController::class, "destroy"]);