<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view("cast.tambah");
    }
    public function store(Request $request)
    {
        $request->validate([
            "nama" => "required|min:5",
            "umur" => "required|max:2",
            "bio" => "required|min:10",
        ]);

        DB::table("cast")->insert([
            "nama" => $request->input("nama"),
            "umur" => $request->input("umur"),
            "bio" => $request->input("bio"),
        ]);

        return redirect("/cast");
    }

    public function index()
    {
        $casts = DB::table("cast")->get();
        return view("cast.tampil", ["casts" => $casts]);
    }

    public function show($id)
    {
        $cast = DB::table("cast")->find($id);
        return view("cast.detail", ["cast" => $cast]);
    }

    public function edit($id)
    {
        $cast = DB::table("cast")->find($id);
        return view("cast.edit", ["cast" => $cast]);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            "nama" => "required|min:5",
            "umur" => "required|max:2",
            "bio" => "required|min:10",
        ]);

        DB::table("cast")
            ->where("id", $id)
            ->update([
                "nama" => $request->input("nama"),
                "umur" => $request->input("umur"),
                "bio" => $request->input("bio"),
            ]);

        return redirect("/cast");
    }

    public function destroy($id)
    {
        DB::table("cast")
            ->where("id", "=", $id)
            ->delete();
        return redirect("/cast");
    }
}