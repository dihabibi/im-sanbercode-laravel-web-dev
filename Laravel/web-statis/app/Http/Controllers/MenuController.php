<?php

namespace App\Http\Controllers;

class MenuController extends Controller
{
    public function dashboard()
    {
        return view('page.dashboard');
    }

    public function table()
    {
        return view('page.table');
    }

    public function dataTables()
    {
        return view('page.data-tables');
    }
}