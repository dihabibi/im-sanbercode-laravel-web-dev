<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>

<body>
    <h1>Berlatih String PHP</h1>
    <?php
    echo "<h3> Soal No 1</h3>";
    /* 
            SOAL NO 1
            Tunjukan dengan menggunakan echo berapa panjang dari string yang diberikan berikut! Tunjukkan juga jumlah kata di dalam kalimat tersebut! 

            Contoh: 
            $string = "PHP is never old";
            Output:
            Panjang string: 16, 
            Jumlah kata: 4 
    */

    $first_sentence = "Hello PHP!"; // Panjang string 10, jumlah kata: 2
    $second_sentence = "I\'m ready for the challenges"; // Panjang string: 28, jumlah kata: 5

    echo "<p>Panjang string: " . strlen($first_sentence) . "</p>";
    echo "<p>Jumlah kata: " . str_word_count($first_sentence) . "</p>";

    echo "<p>Panjang string: " . strlen($second_sentence) . "</p>";
    echo "<p>Jumlah kata: " . str_word_count($second_sentence) . "</p>";

    echo "<h3> Soal No 2</h3>";
    /* 
            SOAL NO 2
            Mengambil kata pada string dan karakter-karakter yang ada di dalamnya. 
        */
    $string2 = "I love PHP";

    echo "<label>String: </label> \"$string2\" <br>";
    echo "<p>Kata pertama: " . substr($string2, 0, strpos($string2, ' ')) . "</p>";

    $firstSpaceIndex = strpos($string2, ' ');
    $secondSpaceIndex = strpos($string2, ' ', $firstSpaceIndex + 1);

    echo "<p>Kata kedua: " . substr($string2, $firstSpaceIndex + 1, $secondSpaceIndex - $firstSpaceIndex - 1) . "</p>";
    echo "<p>Kata ketiga: " . substr($string2, $secondSpaceIndex + 1) . "</p>";


    echo "<h3> Soal No 3 </h3>";
    /*
            SOAL NO 3
            Mengubah karakter atau kata yang ada di dalam sebuah string.
        */
    $string3 = "PHP is old but sexy!";
    echo "<p>String: \"$string3\"</p>";
    echo "<p>Output: \"" . str_replace("sexy", "awesome", $string3) . "\"</p>";
    ?>
</body>

</html>